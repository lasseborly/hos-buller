#OBS! Siden er ikke live endnu!
***

#Hos Buller

######"Hos Buller" is a simple node website without any CMS. 

The app's purpose is to serve HTML/Handlebars files and act as a mail server.

##Technology

* ###[Node.js](https://nodejs.org/)
     
    Used as the runtime envoirement for our JavaScript codebase.   
***

* ###[Express.js](http://expressjs.com/)

    Express is the framework used to easily and painlessly serve the handlebar templates via it's routing.
***

* ###[Nodemailer](http://www.nodemailer.com/)

    Transport module hooking up to gmail's SMTP protocol and slings forth the mail to the designated address.
***

* ###[Handlebars.js](http://handlebarsjs.com/)

    Very simple and highly itterated templating engine. Credit has to be given to this specific module creator [ericf](https://github.com/ericf/express-handlebars). 
***

* ###[Bootstrap](http://getbootstrap.com/)

    Front-end framework used mainly for it's grid and responsive capabilities. 
***

* ###[Slick.js](http://kenwheeler.github.io/slick/)

    Used for the home pages carousel. Looked slick.
***