var express = require('express');
var exphbs  = require('express-handlebars');
var bodyParser = require('body-parser');

var app = express();

app.engine('handlebars', exphbs({defaultLayout: 'main'}));
app.set('view engine', 'handlebars');
app.use(express.static('public'));
app.use(bodyParser.json());
app.use(bodyParser.urlencoded({ extended: true }));
app.enable('view cache');

var nodemailer = require('nodemailer');

//Routes
app.get('/', function (req, res) {
  res.render('home');
});

app.get('/alacarte', function (req, res) {
  res.render('alacarte');
});

app.get('/buffet', function (req, res) {
  res.render('buffet');
});

app.get('/kontakt', function (req, res) {
  res.render('kontakt');
});

app.post('/kontakt', function (req, res) {

  var mailOpts, smtpTrans;

  smtpTrans = nodemailer.createTransport({
      service: 'Gmail',
      auth: {
          user: 'add@mail.com',
          pass: 'password'
      }
  });

  mailOpts = {
    from: req.body.name + ' &lt;' + req.body.email + '&gt;',
    to: 'receiver@mail.com',
    subject: req.body.tel,
    text: req.body.message
  };

  smtpTrans.sendMail(mailOpts, function (error, info) {
    //Email not sent
    if (error) {
      res.render('kontakt', { title: 'Hos Buller - Kontakt', msg: 'Fejl. Mailen blev ikke sendt.', err: true, page: 'kontakt' })
      console.log("Message not sent!");
    }
    //Email sent
    else {
      res.render('kontakt', { title: 'Hos Buller - Kontakt', msg: 'Mail sendt! Tak.', err: false, page: 'kontakt' })
      console.log("Message sent!");
    }
  });

});

app.get('/om', function (req, res) {
  res.render('om');
});

app.get('/julefrokost', function (req, res) {
  res.render('julefrokost');
});

app.get('/grill', function (req, res) {
  res.render('grill');
});

app.listen(3000, function() {
  console.log("Listening on port 3000");
});
